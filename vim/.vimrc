let PLUGREPO = 'https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
let PLUGPATH = $HOME . '/.vim/autoload/plug.vim'
if empty(glob(PLUGPATH))
  execute 'silent !curl --create-dirs -fLo ' . PLUGPATH . ' ' . PLUGREPO
  autocmd VimEnter * PlugInstall | source $MYVIMRC
endif

call plug#begin($HOME . '/.vim/plugged')

Plug 'scrooloose/nerdtree'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'mhinz/vim-grepper'

Plug 'christoomey/vim-tmux-navigator'
Plug 'airblade/vim-gitgutter'
Plug 'scrooloose/nerdcommenter'
Plug 'jiangmiao/auto-pairs'
Plug 'vim-airline/vim-airline'
Plug 'arcticicestudio/nord-vim'
Plug 'kshenoy/vim-signature'
Plug 'wesQ3/vim-windowswap'
Plug 'milkypostman/vim-togglelist'

Plug 'othree/xml.vim'
Plug 'dewyze/vim-tada'

call plug#end()

" --Settings
set nocompatible
let mapleader="\<Space>"

" encoding
set encoding=UTF-8
set fileencoding=UTF-8

" syntax
set background=dark
colorscheme nord
set cursorline
autocmd ColorScheme * hi Folded ctermbg=None
autocmd ColorScheme * hi Comment ctermfg=244

" turn on filetype
filetype plugin indent on

" syntax highlighting
syntax on

" auto indent
set autoindent

" reload on external change
set autoread

" remove vertical bar chars
set fillchars+=vert:\ " trailing space is vital!

" tabs
set tabstop=4
set shiftwidth=4
set expandtab
set smarttab

set number

set foldmethod=syntax
set foldlevelstart=99

set backspace=indent,eol,start
set nowrap

" ruler options
set ruler
set cc=81
hi ColorColumn ctermbg=0

" show invisibiles
set list
set listchars+=eol:¬,extends:>,precedes:<,tab:»\ ,trail:·,nbsp:·

set signcolumn=yes

" splitting
set splitbelow
set splitright

" wrapping
set linebreak

" scroll offset
set so=5

" search highlight
set hlsearch
set incsearch

" smart case in search
set ignorecase
set smartcase

" suggestion in command line
set wildmenu

" mouse support
set mouse=a
map <MiddleMouse> <Nop>
imap <MiddleMouse> <Nop>
map <2-MiddleMouse> <Nop>
imap <2-MiddleMouse> <Nop>
map <3-MiddleMouse> <Nop>
imap <3-MiddleMouse> <Nop>
map <4-MiddleMouse> <Nop>
imap <4-MiddleMouse> <Nop>

" sudo write
command! Suw :w !sudo tee %

" toggle wrap
function! ToggleWrap()
    if (&wrap == 0)
        set wrap
    else
        set nowrap
    endif
endfunction

" nerdtree
let g:NERDTreeDirArrowExpandable = '+'
let g:NERDTreeDirArrowCollapsible = '-'
let g:NERDTreeIndicatorMapCustom = {
    \ "Modified"  : "~",
    \ "Staged"    : "+",
    \ "Untracked" : "+",
    \ "Renamed"   : ">",
    \ "Unmerged"  : "═",
    \ "Deleted"   : "-",
    \ "Dirty"     : "~",
    \ "Clean"     : "✔︎",
    \ "Unknown"   : "?"
    \ }

" ctrlp
let g:ctrlp_show_hidden = 1

" grepper
let g:grepper = {
    \ "tools": ["grep", "git"],
    \ "simple_prompt": 1
    \ }

" tmux nagivator
let g:tmux_navigator_no_mappings = 1

" airline
set noshowmode
let g:airline_powerline_fonts=0
let g:airline#extensions#tabline#show_close_button = 0
let g:airline#extensions#tabline#show_splits = 0
let g:airline#extensions#tabline#show_buffers = 0

" autopairs
let g:AutoPairsMapSpace = 0

" vim-tada
let g:tada_todo_styles = 'markdown'


" --Mappings
" clipboard shortcuts
noremap <Leader>y "+y
noremap <Leader>x "+x
noremap <Leader>p "+p
noremap <Leader>P "+P

" nohl shortcut
nnoremap <silent> <Leader>/ :nohl<cr>

" pane resize shortcut
nnoremap <Leader>> <C-w>10>
nnoremap <Leader>< <C-w>10<
nnoremap <Leader>+ <C-w>10+
nnoremap <Leader>- <C-w>10-

" nerdtree
nnoremap <silent> <Leader>f :NERDTreeToggle<CR>

" grepper
nnoremap <C-F> :Grepper<CR>
nmap gs  <plug>(GrepperOperator)
xmap gs  <plug>(GrepperOperator)

" tmux navigator
if exists("$TMUX")
    nnoremap <silent> h :TmuxNavigateLeft<cr>
    nnoremap <silent> j :TmuxNavigateDown<cr>
    nnoremap <silent> k :TmuxNavigateUp<cr>
    nnoremap <silent> l :TmuxNavigateRight<cr>
else
    nnoremap <silent> h <C-w>h
    nnoremap <silent> j <C-w>j
    nnoremap <silent> k <C-w>k
    nnoremap <silent> l <C-w>l
endif

" nerdcommenter
nmap <C-_> <Plug>NERDCommenterToggle
vmap <C-_> <Plug>NERDCommenterToggle<CR>gv

" toggle wrap
map <silent>  <leader>tl :call ToggleWrap()<cr>

" ctrl-s save
" WARNING: use `stty -ixon` in .zshrc
map <silent> <C-s> :w<cr>
imap <silent> <C-s> :w<cr>a
