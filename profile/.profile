source /etc/profile

export EDITOR="vim"
export PAGER="less"

# ssh agent
if [ -d ~/.ssh ]; then
    pgrep -u "$USER" ssh-agent >/dev/null || ssh-agent >~/.ssh/environment
    [ -z "$SSH_AGENT_PID" ] && eval "$(cat ~/.ssh/environment)"
fi

[ -e ~/.profile-dev ] && . ~/.profile-dev

export PATH="${HOME}/.local/bin:${PATH}"
export MANPATH="${HOME}/.local/share/man:${MANPATH}"
