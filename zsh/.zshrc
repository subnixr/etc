# colors
MAIN_COLOR="2"

export GREP_COLOR="0;3${MAIN_COLOR}"

# minimal
MNML_USER_CHAR="λ"
MNML_OK_COLOR="$MAIN_COLOR"
MNML_RPROMPT=('mnml_cwd 2 10' mnml_git)
ZSH_MINIMAL="$HOME/.minimal.zsh"
ZSH_MINIMAL_REMOTE="https://raw.githubusercontent.com/subnixr/minimal/master/minimal.zsh"
[ ! -e "$ZSH_MINIMAL" ] && curl -L -o "$ZSH_MINIMAL" "$ZSH_MINIMAL_REMOTE"
source "$ZSH_MINIMAL"

# python virtual env
export VIRTUAL_ENV_DISABLE_PROMPT=true

# zsh-syntax-highlighting
ZSH_SYNTAX_HIGHLIGHTING=""
# debian based
if [ -e "/usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh" ]; then
    ZSH_SYNTAX_HIGHLIGHTING="/usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"
# arch based
elif [ -e "/usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh" ]; then
    ZSH_SYNTAX_HIGHLIGHTING="/usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"
fi

if [ -n "$ZSH_SYNTAX_HIGHLIGHTING" ]; then 
    typeset -A ZSH_HIGHLIGHT_STYLES
    ZSH_HIGHLIGHT_HIGHLIGHTERS=(main)

    ZSH_HIGHLIGHT_STYLES[unknown-token]="fg=red"
    ZSH_HIGHLIGHT_STYLES[reserved-word]="fg=$MAIN_COLOR"
    ZSH_HIGHLIGHT_STYLES[alias]="fg=$MAIN_COLOR"
    ZSH_HIGHLIGHT_STYLES[builtin]="fg=$MAIN_COLOR"
    ZSH_HIGHLIGHT_STYLES[function]="fg=$MAIN_COLOR"
    ZSH_HIGHLIGHT_STYLES[command]="fg=$MAIN_COLOR"
    ZSH_HIGHLIGHT_STYLES[precommand]="underline,fg=$MAIN_COLOR"
    ZSH_HIGHLIGHT_STYLES[commandseparator]="fg=blue"
    ZSH_HIGHLIGHT_STYLES[hashed-command]="fg=$MAIN_COLOR"
    ZSH_HIGHLIGHT_STYLES[globbing]="fg=yellow"
    ZSH_HIGHLIGHT_STYLES[single-hyphen-option]="fg=$((MAIN_COLOR + 8))"
    ZSH_HIGHLIGHT_STYLES[double-hyphen-option]="fg=$((MAIN_COLOR + 8))"
    ZSH_HIGHLIGHT_STYLES[single-quoted-argument]="fg=yellow"
    ZSH_HIGHLIGHT_STYLES[double-quoted-argument]="fg=yellow"
    ZSH_HIGHLIGHT_STYLES[dollar-double-quoted-argument]="fg=blue"
    ZSH_HIGHLIGHT_STYLES[redirection]="fg=$((MAIN_COLOR + 8))"
    ZSH_HIGHLIGHT_STYLES[arg0]="fg=$MAIN_COLOR"

    export ZSH_HIGHLIGHT_STYLES
    export ZSH_HIGHLIGHT_HIGHLIGHTERS
    source "$ZSH_SYNTAX_HIGHLIGHTING"
fi

# colored man
export LESS_TERMCAP_mb="$(printf "\e[0;3${MAIN_COLOR}m")"    # start blink
export LESS_TERMCAP_md="$(printf "\e[0;3${MAIN_COLOR}m")"    # start bold
export LESS_TERMCAP_so="$(printf "\e[7;3${MAIN_COLOR}m")"    # start standout
export LESS_TERMCAP_us="$(printf "\e[1;4;3${MAIN_COLOR}m")"  # start underline
export LESS_TERMCAP_me="$(printf "\e[0m")"                   # stop blink, bold
export LESS_TERMCAP_se="$(printf "\e[0m")"                   # stop standout
export LESS_TERMCAP_ue="$(printf "\e[0m")"                   # stop underline

# no history duplicates
setopt INC_APPEND_HISTORY
setopt SHARE_HISTORY
setopt EXTENDED_HISTORY
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_IGNORE_SPACE

# case insensitive completition
autoload -U compinit && compinit
zstyle ':completion:*' menu select=2
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'

# vimode
bindkey -v
bindkey '^P' up-history
bindkey '^N' down-history
bindkey '^?' backward-delete-char
#bindkey '^h' backward-delete-char
#bindkey '^w' backward-kill-word
bindkey '^r' history-incremental-search-backward
bindkey -M vicmd '/' history-incremental-search-backward
bindkey -M vicmd 'j' history-beginning-search-forward
bindkey -M vicmd 'k' history-beginning-search-backward
bindkey -M vicmd 'K' run-help

# bind C-Z to restore
function restore() {
    BUFFER="fg"
    zle accept-line
}
zle -N restore
bindkey -M main  "^Z" restore
bindkey -M vicmd "^Z" restore

export KEYTIMEOUT=1

# pyenv
if command -v pyenv >/dev/null; then
    eval "$(pyenv init -)"
    eval "$(pyenv virtualenv-init -)"
fi

# aliases
alias ls="ls -F --color=auto"
alias l="ls -lah"
alias grep="grep --color=auto"

alias path='printf "${PATH//:/\\n}\n"'
alias hist='fc -l 1'

alias em="emacsclient --create-frame --chdir ."

alias py="python3"
alias ipy="ipython3 --no-confirm-exit"
alias pdb="python3 -m pdb"
alias workon="source venv/bin/activate"
alias mkvenv="python3 -m venv"

# disable ctrl-s,ctrl-q
[[ -o interactive ]] && stty -ixon

titlebar() {
    printf "\033]0;%s@%s:%s %s %s\007" "${USER}" "${HOST%%.*}" "${PWD/#$HOME/~}" "$2";
}

precmd_functions=(titlebar)
preexec_functions=(titlebar)
