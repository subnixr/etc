let CONFIGS = $XDG_CONFIG_HOME
if CONFIGS == ""
    let CONFIGS = $HOME . "/.config"
endif

let PLUGREPO = 'https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
let PLUGPATH = $HOME . '/.vim/autoload/plug.vim'
if has('nvim')
    let PLUGPATH = CONFIGS . '/nvim/autoload/plug.vim'
endif

if empty(glob(PLUGPATH))
  execute 'silent !curl --create-dirs -fLo ' . PLUGPATH . ' ' . PLUGREPO
  autocmd VimEnter * PlugInstall | source $MYVIMRC
endif

call plug#begin(CONFIGS . '/nvim/plugged')

Plug 'ncm2/ncm2'
Plug 'roxma/nvim-yarp'
Plug 'ncm2/ncm2-bufword'
Plug 'ncm2/ncm2-path'
Plug 'autozimu/LanguageClient-neovim'

Plug 'neomake/neomake'
Plug 'scrooloose/nerdtree'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'mhinz/vim-grepper'

Plug 'christoomey/vim-tmux-navigator'
Plug 'airblade/vim-gitgutter'
Plug 'scrooloose/nerdcommenter'
Plug 'jiangmiao/auto-pairs'
Plug 'vim-airline/vim-airline'
Plug 'arcticicestudio/nord-vim'
Plug 'kshenoy/vim-signature'
Plug 'wesQ3/vim-windowswap'
Plug 'milkypostman/vim-togglelist'

Plug 'othree/xml.vim'

call plug#end()

" --Settings
set nocompatible
let mapleader="\<Space>"

" encoding
set encoding=UTF-8
set fileencoding=UTF-8

" syntax
set background=dark
colorscheme nord
set cursorline
autocmd ColorScheme * hi Folded ctermbg=None
autocmd ColorScheme * hi Comment ctermfg=244

" turn on filetype
filetype plugin indent on
syntax on

" reload on external change
set autoread

" remove vertical bar chars
set fillchars+=vert:\ " trailing char is vital!

" tabs
set tabstop=4
set shiftwidth=4
set expandtab
set smarttab

set number

set foldmethod=syntax
set foldlevelstart=99

set backspace=indent,eol,start
set nowrap

" ruler options
set ruler
set cc=81
hi ColorColumn ctermbg=0

" show invisibiles
set list
set listchars+=eol:¬,extends:>,precedes:<,tab:»\ ,trail:·,nbsp:·

set signcolumn=yes

" splitting
set splitbelow
set splitright

" wrapping
set linebreak

" scroll offset
set so=5

" search highlight
set hlsearch
set incsearch

" incremental substitute
if has('nvim')
    set inccommand=split
endif

" smart case in search
set ignorecase
set smartcase

set autoindent
set wildmenu

" lsp support
set hidden

" mouse support
set mouse=a
map <MiddleMouse> <Nop>
imap <MiddleMouse> <Nop>
map <2-MiddleMouse> <Nop>
imap <2-MiddleMouse> <Nop>
map <3-MiddleMouse> <Nop>
imap <3-MiddleMouse> <Nop>
map <4-MiddleMouse> <Nop>
imap <4-MiddleMouse> <Nop>

" sudo write
command! Suw :w !sudo tee %

" toggle wrap
function! ToggleWrap()
    if (&wrap == 0)
        set wrap
    else
        set nowrap
    endif
endfunction

" ncm2
autocmd BufEnter * call ncm2#enable_for_buffer()
" IMPORTANT: :help Ncm2PopupOpen for more information
set completeopt=noinsert,menuone,noselect

" LSP
let g:LanguageClient_serverCommands = {
       \ 'go': ['gopls'],
       \ 'javascript': ['javascript-typescript-stdio'],
       \ }

" Run gofmt and goimports on save
autocmd BufWritePre *.go :call LanguageClient#textDocument_formatting_sync()

" neomake
autocmd! BufWritePost * Neomake
let g:neomake_open_list = 2
let g:neomake_warning_sign = {'text': 'W', 'texthl': 'WarningMsg'}
let g:neomake_error_sign = {'text': 'E', 'texthl': 'ErrorMsg'}
let g:neomake_python_enabled_makers = []

" nerdtree
let g:NERDTreeDirArrowExpandable = '+'
let g:NERDTreeDirArrowCollapsible = '-'
let g:NERDTreeIndicatorMapCustom = {
    \ "Modified"  : "~",
    \ "Staged"    : "+",
    \ "Untracked" : "+",
    \ "Renamed"   : ">",
    \ "Unmerged"  : "═",
    \ "Deleted"   : "-",
    \ "Dirty"     : "~",
    \ "Clean"     : "✔︎",
    \ "Unknown"   : "?"
    \ }

" ctrlp
let g:ctrlp_show_hidden = 1

" grepper
let g:grepper = {
    \ "tools": ["grep", "git"],
    \ "simple_prompt": 1
    \ }

" tmux nagivator
let g:tmux_navigator_no_mappings = 1

" airline
set noshowmode
let g:airline_powerline_fonts=0
let g:airline#extensions#tabline#show_close_button = 0
let g:airline#extensions#tabline#show_splits = 0
let g:airline#extensions#tabline#show_buffers = 0

" autopairs
let g:AutoPairsMapSpace = 0

" --Mappings
" clipboard shortcuts
noremap <Leader>y "+y
noremap <Leader>x "+x
noremap <Leader>p "+p
noremap <Leader>P "+P

" nohl shortcut
nnoremap <silent> <Leader>/ :nohl<cr>

" pane resize shortcut
nnoremap <Leader>> <C-w>10>
nnoremap <Leader>< <C-w>10<
nnoremap <Leader>+ <C-w>10+
nnoremap <Leader>- <C-w>10-

" nerdtree
nnoremap <silent> <Leader>f :NERDTreeToggle<CR>
nnoremap <silent> <F2> :NERDTreeFind<CR>

" grepper
nnoremap <C-F> :Grepper<CR>
nmap gs  <plug>(GrepperOperator)
xmap gs  <plug>(GrepperOperator)

" ncm2
" insert line after accept suggestion
inoremap <expr> <CR> (pumvisible() ? "\<c-y>\<cr>" : "\<CR>")
inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

" LSP
nnoremap <silent> K :call LanguageClient#textDocument_hover()<CR>
nnoremap <silent> gd :call LanguageClient#textDocument_definition()<CR>
nnoremap <silent> <F2> :call LanguageClient#textDocument_rename()<CR>

" tmux navigator
nnoremap <silent> <A-h> :TmuxNavigateLeft<cr>
nnoremap <silent> <A-j> :TmuxNavigateDown<cr>
nnoremap <silent> <A-k> :TmuxNavigateUp<cr>
nnoremap <silent> <A-l> :TmuxNavigateRight<cr>

" nerdcommenter
nmap <C-_> <Plug>NERDCommenterToggle
vmap <C-_> <Plug>NERDCommenterToggle<CR>gv

" toggle wrap
map <silent>  <leader>wt :call ToggleWrap()<cr>
